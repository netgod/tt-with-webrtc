<?php if (!defined('THINK_PATH')) exit();?><form id="pagerForm" action="__URL__" method="post">
	<input type="hidden" name="pageNum" value="<?php echo (($_REQUEST['pageNum'])?($_REQUEST['pageNum']):1); ?>"/>
	<input type="hidden" name="medium" value="<?php echo ($_REQUEST["medium"]); ?>"/>
	<input type="hidden" name="name" value="<?php echo ($_REQUEST['name']); ?>"/>
	<input type="hidden" name="title" value="<?php echo ($_REQUEST['title']); ?>"/>
</form>

<div class="page">
	<div class="pageHeader">
		<form onsubmit="return navTabSearch(this);" action="__URL__" method="post">
		<div class="searchBar">
			<ul class="searchContent">
				<li>
					<label>名称：</label>
					<input type="text" name="name" class="name" value="<?php echo ($_REQUEST['name']); ?>">
				</li>
				<li>
					<label>标示：</label>
					<input type="text" name="title" class="title" value="<?php echo ($_REQUEST['title']); ?>">
				</li>
			</ul>
			<div class="subBar">
				<ul>
					<li><div class="buttonActive"><div class="buttonContent"><button type="submit">查询</button></div></div></li>
				</ul>
			</div>
		</div>
		</form>
	</div>
	
	<div class="pageContent">
		<div class="panelBar">
			<ul class="toolBar">
				<li><a class="add" href="__URL__/add" target="dialog" mask="true"><span>新增</span></a></li>
				<li><a class="delete" href="__URL__/foreverdelete/id/{sid_node}/navTabId/__MODULE__" target="ajaxTodo" calback="navTabAjaxMenu" title="你确定要删除吗？" warn="请选择节点"><span>删除</span></a></li>
				<li><a class="edit" href="__URL__/edit/id/{sid_node}" target="dialog" mask="true" warn="请选择节点"><span>修改</span></a></li>
			</ul>
		</div>


		<table class="list" width="100%" layoutH="116">
			<thead>
			<tr>
				<th width="60">编号</th>
				<th width="100">名称</th>
				<th width="100">所属导航</th>
				<th width="100">标识</th>
				<th width="80">序号</th>
				<th width="100">状态</th>
				<th width="100">操作</th>
			</tr>
			</thead>
			<tbody>
			<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr target="sid_node" rel="<?php echo ($vo['id']); ?>">
					<td><?php echo ($vo['id']); ?></td>
					<td><a href="__URL__/edit/id/<?php echo ($vo['id']); ?>" target="dialog"><?php echo ($vo['name']); ?></a></td>
					<td><?php echo ($menu[$vo['group_menu']]); ?></td>
					<td><?php echo ($vo['title']); ?></td>
					<td><?php echo ($vo['sort']); ?></td>
					<td><?php echo (getstatus($vo['status'])); ?></td>
					<td><?php echo (showstatus($vo['status'],$vo['id'],'navTabAjaxMenu')); ?> <a href="__URL__/edit/id/<?php echo ($vo['id']); ?>" target="dialog">编辑</a></td>
				</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>

		<div class="panelBar">
			<div class="pages">
				<span>共<?php echo ($totalCount); ?>条</span>
			</div>
			<div class="pagination" targetType="navTab" totalCount="<?php echo ($totalCount); ?>" numPerPage="<?php echo ($numPerPage); ?>" pageNumShown="10" currentPage="<?php echo ($currentPage); ?>"></div>
		</div>
	</div>
</div>