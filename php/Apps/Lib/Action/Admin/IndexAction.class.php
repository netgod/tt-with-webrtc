<?php

class IndexAction extends CommonAction {

    // 框架首页
    public function index() {

        if (isset($_SESSION [C('USER_AUTH_KEY')])) {
            //luz start
            $volist = M("GroupClass")->where(array('status' => 1))->order("sort desc, id desc")->select();
            $this->volist = $volist;
            //luz end
            //显示菜单项
            $menu = array();

            //读取数据库模块列表生成菜单项
            $node = M("Node");
            $id = $node->getField("id");
            $where ['level'] = 2;
            $where ['status'] = 1;
            $where ['pid'] = $id;
            $list = $node->where($where)->field('id,name,group_id,title')->order('sort asc')->select();
            $accessList = $_SESSION ['_ACCESS_LIST'];
            foreach ($list as $key => $module) {
                if (isset($accessList [strtoupper(APP_NAME)] [strtoupper($module ['name'])]) || $_SESSION ['administrator']) {
                    //设置模块访问权限
                    $module ['access'] = 1;
                    //lxz 修改 获取当前分类的module
                    $menu[$module['group_id']] [$key] = $module;
                }
            }

            if (!empty($_GET ['tag'])) {
                $this->assign('menuTag', $_GET ['tag']);
            }

    
            $groups = M("GroupAdmin")->where(array('group_menu' => "{$volist[0]['menu']}", 'status' => "1"))->order("sort desc,id desc")->select();
            $this->assign("groups", $groups);
            $this->assign('menu', $menu);

            //显示首页通知信息
            $Yw_db = M('Notice');
            $map['status'] = "1";
            $this->_list($Yw_db, $map);
            
            //显示统计信息
            
            
            
        }
        C('SHOW_RUN_TIME', false); // 运行时间显示
        C('SHOW_PAGE_TRACE', false);
        $this->display();
    }

}

?>