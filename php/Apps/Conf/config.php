<?php

return array(
      /*
     * 0:普通模式 (采用传统癿URL参数模式 )
     * 1:PATHINFO模式(http://<serverName>/appName/module/action/id/1/)
     * 2:REWRITE模式(PATHINFO模式基础上隐藏index.php)
     * 3:兼容模式(普通模式和PATHINFO模式, 可以支持任何的运行环境, 如果你的环境不支持PATHINFO 请设置为3)
     */
    'URL_MODEL' => 2,
    'DB_TYPE' => 'mysql', 
    'DB_HOST' => '172.16.211.60',
    'DB_NAME' => 'teamtalk',
    'DB_USER' => 'root',
    'DB_PWD' => 'weixiao123', 
    'DB_PORT' => '3306',
    'DB_PREFIX' => 'IM', 
    
    //'配置项'=>'配置值'
    'APP_GROUP_LIST' => 'Api,Home,Admin', //项目分组设定
    'DEFAULT_GROUP' => 'Home', //默认分组
    'TMPL_ACTION_ERROR' => '../dispatch_jump', // 错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS' => '../dispatch_jump', // 成功跳转对应的模板文件
    //开启url大小写无视
    'URL_CASE_INSENSITIVE' => true
);
?>